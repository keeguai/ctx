﻿// 新标签打开网页
$('#open_url_new_tab').click(() => {
	chrome.tabs.create({url: 'http://192.168.8.20:88/redmine/login'});
});
// 获取background
let bg = chrome.extension.getBackgroundPage();
$('#set_user').click(() => {
	let isShow = $('form#user').attr('isShow');
	if($('form#user').attr('isShow')=='true'){
		let Username = $('#Username').val();
		let Password = $('#Password').val();
		if(Username.length == 0 || Password.length == 0){
			layer.alert('账号密码不能为空！', {
				icon: 2,
				skin: 'layui-layer-lan', //样式类名
				closeBtn: 0
			});
			return;
		}
		// 保存用户账号密码
		bg.saveUser([Username,Password]).then((result) => {
			if(result == 0){
				layer.alert('账号信息验证成功，将会开始自动获取问题更新！', {
					icon: 1,
					skin: 'layui-layer-lan', //样式类名
					closeBtn: 0
				});
				// 登录验证通过后开始循环获取最新问题列表
				bg.loopIssues();
				$('form#user').hide();
				$('form#user').attr('isShow','false');
				$('#set_user').text('设置自动登陆');
			}else{
				layer.alert('账号信息登录验证失败，请确认后重新输入账号密码！', {
					icon: 2,
					skin: 'layui-layer-lan', //样式类名
					closeBtn: 0
				});
			}
		});
		// chrome.runtime.sendMessage({msg: [Username,Password]}, function(response) {
		// 	console.log('收到来自后台的回复：' + response);
		// });
		// 	chrome.storage.sync.set({username: Username,password:Password}, function() {
		// 		console.log('保存成功！');
		//   });	
	}else{
		//  bg.remUser();
		(async ()=>{
			let user ;
			await bg.getUser().then((items)=>{
				user = items;
			});
			// 用户为空，直接显示账号密码输入
			if(user == null || user.username == null){
				console.log('未发现用户信息，停止信息获取');
				$('form#user').show();
				$('form#user').attr('isShow','true');
				$('#set_user').text('保存自动登陆');
			}else{
				// 弹出确认框
				console.log(user);
				layer.alert('已有账号 '+user.username+' 登录，确认要切换账号吗？', {
					icon: 0,
					skin: 'layui-layer-lan', //样式类名
					btn: ['确认', '取消'],
					yes: function(index){
						$('form#user').show();
						$('form#user').attr('isShow','true');
						$('#set_user').text('保存自动登陆');
						layer.close(index);
					}
				});
			}
		})();
		
		
	}
});

  