chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status == "complete") {
        var url = tab.url;
        // console.log("用户访问：" + url);

        // console.log("缓存里设置的页面：" + localStorage.url);
    };
});

// 监听来自content-script的消息
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
{
  let login = /\/login/;
  // 如果是来自login的消息
  if(login.test(sender.url)){
    (async ()=>{
      let isLogin = await saveUser([request.username,request.password]);
      if(isLogin === 0){
        // 如果登录成功就保存账号密码
        await setUser([request.username,request.password])
      }
      sendResponse({name:isLogin});
    })();
  }
  // 保持消息通道不关闭直到sendResponse方法被调用
  return true;
	// sendResponse('123'+JSON.stringify('登陆'));
});

/**
 * 提供方法给popup.js调用，保存用户信息
 * @param {*} msg 
 */
async function saveUser(msg){
    let user = {username: msg[0],password: msg[1]};
    // 登录验证账号密码是否正确
    let cu = await checkUser(msg);
    if(cu == 1){
      // 登录失败，清空用户账号信息
      // let ru = await remUser();
      return 1;
    }else{
      // 登录验证通过，保存用户数据
      let su = await setUser(msg);
      return 0;
    }
}

/**
 * 验证账号密码是否正确
 * @param {[username,password]} msg 
 */
async function checkUser(msg){
  // get请求获取登录页的token
  let token = $(await loginGet()).filter('meta[name="csrf-token"]').attr('content');
  // 使用token和账号密码登录
  let i = $(await loginPost(token,msg[0],msg[1]));
  if(i.find('div#loggedas').length > 0 && i.find('div#flash_error').length==0){
    console.log('账号 '+msg[0]+' 登录成功！');
    return 0;
  }else{
    console.log('账号 '+msg[0]+' 登录失败');
    return 1;
  }
}
/**
 * 保存用户信息
 * @param {账号密码数组} msg 
 */
function setUser(msg){
  return new Promise((resolve,reject) => {
    // 读取数据，第一个参数是指定要读取的key以及设置默认值
    chrome.storage.sync.set({username: msg[0],password: msg[1]},() => {
      console.log('用户信息保存成功！');
      return resolve(1);
    });
  })
}
/**
 * 获取用户信息
 */
function getUser(){
  return new Promise((resolve,reject) => {
      // 读取数据，第一个参数是指定要读取的key以及设置默认值
    chrome.storage.sync.get(['username','password'], function(user) {
      return resolve(user);
    });
  })
}
/**
 * 移除用户数据
 */
function remUser(){
  return new Promise((resolve,reject) => {
    // 移除用户数据
    chrome.storage.sync.remove(['username','password'], function() {
      return resolve(1);
    });
  })
}
  /**
   * 1、请求问题列表
   * 2、判断是否还在登陆状态 获取页面菜单栏是否有我的账号
   * 3、如果是未登录，那就执行登陆
   * 4、如果是已登录，那就获取localstorage最新一条问题的时间
   * 5、localstorage为空，那就将最新一条存入
   * 6、localstorage有值，那就将本业全部问题时间进行匹配,将最新的都存入数组中
   * 6、如果数组条数大于1，那就提示一条信息，存在超过多少个新问题
   * 7、如果数组条数只有1，那就提示发现1个新问题
   */
  async function updateIssue(user){
    // 请求问题页面
    let s = $(await getIssues());
    // 判断是否已登录 不等于0即登陆
    let islogin = s.find('div#loggedas').length;
    if(islogin==0){
      console.log('未登录');
      // get请求获取登录页的token
      let token = $(await loginGet()).filter('meta[name="csrf-token"]').attr('content');
      // 使用token和账号密码登录
      let i = $(await loginPost(token,user.username,user.password));
      if(i.find('div#loggedas').length > 0){
        console.log('登录成功！');
        // 登录成功后重新获取问题清单
        s = $(await getIssues());
      }else{
        console.log('登录失败，停止运行');
        return await sleep(1000*60);
      }
    }
    // 定义问题数组
    let issues = new Array();
    // 定义问题对象构造器
    function issue (id,href,project,status,subject,updated,author){
      this.id = id;
      this.href = href;
      this.project = project;
      this.status = status;
      this.subject = subject;
      this.updated = updated;
      this.author = author;
    }
    // 获取sessionstorage缓存的
    let issueLocal = JSON.parse(localStorage.getItem("issue"));
    // 解析问题列表
    s.find('tr.issue').each(function(){
      let id = $(this).find('td.id>a').text();
      let href = $(this).find('td.id>a').attr('href');
      let project = $(this).find('td.project>a').text();
      let status = $(this).find('td.status').text();
      let subject = $(this).find('td.subject>a').text();
      let updated = $(this).find('td.updated_on').text();
      let author = $(this).find('td.author>a').text();
      let i = new issue(id,href,project,status,subject,updated,author);
      // 如果缓存为空，就塞入一个初始值
      if(issueLocal == null){
        // console.log('null');
        localStorage.setItem("issue",JSON.stringify(i));
      }
      issueLocal = JSON.parse(localStorage.getItem("issue"));
      // 如果获取到的缓存问题时间小于当前问题时间，就再塞入缓存，并加入数组列表中
      if(compareDate(updated,issueLocal.updated)){
        // console.log(updated,issueLocal.updated);
        issues.push(i);
        localStorage.setItem("issue",JSON.stringify(i));
      }
    });
    // console.log(issues);
    let body = '';
    let url = '';
    // 判断更新问题条数，如果只有1条，就直接弹出提醒
    if(issues.length==1){
      body = '编号：'+issues[0].id+'  作者：'+issues[0].author+'\n标题：'+issues[0].subject;
      url = 'http://192.168.8.20:88'+issues[0].href;
    }else if(issues.length>0){
      body = '您有'+issues.length+'条待处理问题，请尽快处理。';
      url = 'http://192.168.8.20:88/redmine/issues?assigned_to_id=me&set_filter=1&sort=updated_on:desc,priority:desc';
    }
    if(body.length>0){
      let n = new Notification('Redmine提醒', {
        icon: 'img/tips.png',
        body: body,
        data: {
          url: url
        }
      });
      //onshow函数在消息框显示时会被调用  
      //可以做一些数据记录及定时操作等  
      // n.onshow = function() {
      //   console.log('显示通知信息');
      //   //5秒后自动关闭消息框  
      //   setTimeout(function() {
      //     n.close();
      //   }, 3000);
      // };
  
      //消息框被点击时被调用  
      //可以打开相关的视图，同时关闭该消息框等操作  
      n.onclick = function() {
        window.open(n.data.url, '_blank');
        n.close();
      };
    }else{
      // Notifier.Notify("img/tips.png", "通知", body);
      console.log('未发现新问题！');
    }
    
    return await sleep(1000*60);
  }
  // 请求我的问题列表，按照时间排序
  function getIssues(){
    return new Promise(function (resolve, reject) {
      $.get('http://192.168.8.20:88/redmine/issues?assigned_to_id=me&set_filter=1&sort=updated_on:desc,priority:desc',function(data){
      // console.log(data);  
      return resolve(data);
      });
    });
  }
  // 登录
  function loginGet(){
    return new Promise(function (resolve, reject) {
      $.get('http://192.168.8.20:88/redmine/login',function(data){
        return resolve(data);
      });
    });
  }
  // 发起登录请求
  function loginPost(token,username,password){
    return new Promise(function (resolve, reject) {
      $.post('http://192.168.8.20:88/redmine/login', 
      { 
        utf8:"✓",
        authenticity_token: token, 
        back_url: "http://192.168.8.20:88/redmine/", 
        username:username, 
        password:password, 
        login:"登录 »"
      },
      function(data){
        // console.log(data);
        return resolve(data);
      });
    });
  }
  
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


  //死循环调用更新方法
  async function loopIssues(){
    let user ;
    // 从浏览器插件存储中获取用户信息
    await getUser().then(items=>{
      user = items;
    });
    if(user == null || user.username == null){
      console.log('未发现用户信息，停止信息获取');
      return;
    }
    // console.log(user);
    let i = 0;
    while(1==1){
      await updateIssue(user);
      // console.log('本次循环结束');
    }
  };
  loopIssues();
  /**
   * 日期比较
   * @param {*} a 
   * @param {*} b 
   * 如果a>b true
   * 如果a<b false
   */
  function compareDate(a,b){
    if(Date.parse(a)>Date.parse(b)){
      return true
    }
    return false;
  }



//  /**
//    * 定时器定时轮询调用
//    */
//   async function test() {
//     let n = new Notification('Redmine提醒', {
//       icon: 'img/tips.png',
//       body: '问题提醒123',
//       data: {
//         url: 'http://192.168.8.20:88/redmine/issues/12283'
//       }
//     });
//     //onshow函数在消息框显示时会被调用  
//     //可以做一些数据记录及定时操作等  
//     n.onshow = function() {
//       console.log('显示通知信息');
//       //5秒后自动关闭消息框  
//       setTimeout(function() {
//         n.close();
//       }, 3000);
//     };

//     //消息框被点击时被调用  
//     //可以打开相关的视图，同时关闭该消息框等操作  
//     n.onclick = function() {
//       window.open(n.data.url, '_blank');
//       n.close();
//     };
//     return new Promise(resolve => setTimeout(resolve, 5000));
//   }

//   (async ()=>{
// 	while(1==1){
// 		await test();
// 	}
//   })();
//   // Conditionally initialize the options.
//   if (!localStorage.isInitialized) {
//     localStorage.isActivated = true;   // The display activation.
//     localStorage.frequency = 1;        // The display frequency, in minutes.
//     localStorage.isInitialized = true; // The option initialization.
//   }
  
//   // Test for notification support.
//   if (window.Notification) {
//     // While activated, show notifications at the display frequency.
//     if (JSON.parse(localStorage.isActivated)) { show(); }
  
//     var interval = 0; // The display interval, in minutes.
  
//     setInterval(function() {
//       interval++;
  
//       if (
//         JSON.parse(localStorage.isActivated) &&
//           localStorage.frequency <= interval
//       ) {
//         show();
//         interval = 0;
//       }
//     }, 60000);
//   }
